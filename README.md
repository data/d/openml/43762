# OpenML dataset: European-Soccer-Dataset-by-Role

https://www.openml.org/d/43762

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A modified version of the European Soccer Database by Hugo Mathien (https://www.kaggle.com/hugomathien). New players' performance indicators have been created from the original dataset.
Data features:
    + 25,000 matches
    11 European Countries with their lead championship
    Seasons from 2008 to 2016
    25 role-based Performance Indicators: players and teams' attributes (sourced from EA Sports' FIFA video game series, including the weekly updates) have been subtracted by location (home - away) and averaged by match and role (X, Y coordinates of the team line up).
Acknowledgements
This dataset has been created in the academic research context and can be used without additional permissions or fees. The newly created indicators have been used to test the performance of several predictive models.
For more information about how the data have been treated and modeled, as well as if you like to use these data in a publication, presentation, or other research product, please consult/use the following citation:
Carpita, M., Ciavolino, E.,  Pasca, P. (2019). Exploring and modelling team performances of the Kaggle European Soccer database. Statistical Modelling, 19(1), 74-101.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43762) of an [OpenML dataset](https://www.openml.org/d/43762). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43762/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43762/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43762/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

